from django.shortcuts import render, redirect, reverse, get_object_or_404
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


@login_required
def projects(request):
    if request.user.is_authenticated:
        projects = Project.objects.filter(
            owner=request.user,
        )
        context = {
            "projects": projects,
        }
        return render(request, "projects/projects.html", context)
    else:
        return redirect(reverse("login"))


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id, owner=request.user)
    context = {
        "project": project,
    }
    return render(request, "projects/project_details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create_project.html", context)
